 import javafx.application.Application;
 import javafx.stage.Stage;
 import javafx.scene.layout.GridPane; //adds the GridPane layout
 import javafx.geometry.Pos; //allows alignment
 import javafx.scene.control.*;
 import javafx.scene.paint.Color; 
 import javafx.scene.Scene;

public class Investment extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {   
    	 GridPane investmentPane = new GridPane();
    	 // Position tipPane for center
    	 investmentPane.setAlignment(Pos.CENTER);
    	 // Set spaces between controls
    	 investmentPane.setVgap(10); // Veritical spacing to 10
    	 investmentPane.setHgap(5); // Horizontal spacing to 5

    	 TextField tfAmount = new TextField();
    	 TextField tfInterest = new TextField();
    	 TextField tfYears = new TextField();
    	 TextField tfResult = new TextField();
    	 
    	 tfResult.setEditable(false);
    	 
    	 tfAmount.setAlignment(Pos.BOTTOM_RIGHT);
    	 tfInterest.setAlignment(Pos.BOTTOM_RIGHT);
    	 tfYears.setAlignment(Pos.BOTTOM_RIGHT);
    	 tfResult.setAlignment(Pos.BOTTOM_RIGHT);

    	 Label lbAmount = new Label("Investment Amount");
    	 Label lbInterest = new Label("Annual Interest Rate");
    	 Label lbYears = new Label("Number of Years");
    	 Label lbResult = new Label("Future Value");
    	 
 		lbResult.setTextFill(Color.RED);
 		tfResult.setText("Please fill in the above.");
    	 
    	 tfAmount.textProperty().addListener( e -> 
    	 calculate(tfAmount.getText(), tfYears.getText(), tfInterest.getText(), tfResult, lbResult));
    	 tfInterest.textProperty().addListener( e -> 
    	 calculate(tfAmount.getText(), tfYears.getText(), tfInterest.getText(), tfResult, lbResult));
    	 tfYears.textProperty().addListener( e -> 
    	 calculate(tfAmount.getText(), tfYears.getText(), tfInterest.getText(), tfResult, lbResult));
    	 
    	 tfAmount.setOnMouseClicked(e -> 
    	 {
    		 tfAmount.selectAll();
    	 });
    	 tfInterest.setOnMouseClicked(e ->
    	 {
    		 tfInterest.selectAll();
    	 });
    	 tfYears.setOnMouseClicked(e ->
    	 {
    		 tfYears.selectAll();
    	 });
    	 
    	 investmentPane.add(lbAmount, 0 , 0);
    	 investmentPane.add(tfAmount, 1 , 0);
    	 investmentPane.add(lbInterest, 0 , 1);
    	 investmentPane.add(tfInterest, 1 , 1);
    	 investmentPane.add(lbYears, 0 , 2);
    	 investmentPane.add(tfYears, 1 , 2);
       	 investmentPane.add(lbResult, 0, 3);
       	 investmentPane.add(tfResult, 1, 3);
    	 
    	 Scene investmentScene = new Scene(investmentPane, 500, 300);
    	 
    	 primaryStage.setTitle("Investment");
    	 primaryStage.setScene(investmentScene); 
    	 
    	 primaryStage.show();
    }
    
    public static void main(String[] args) {
    	 launch(args);
    }
    
    public void calculate(String a, String y, String i, TextField tfResult, Label lbResult) {
    	double amount = 0;
    	double years = 0;
    	double interest = 0;
    	
    	if (!a.isEmpty() && !y.isEmpty() && !i.isEmpty()) {
    		
        	amount = parseDoubleIfPossible(a);	
    		years = parseDoubleIfPossible(y); 
    		interest = parseDoubleIfPossible(i);
    	}
    	
    	//Only if all fields have valid non zero input do we calculate interest rate.
    	if (amount != 0 && years != 0 && interest != 0) {
    		double result = amount * Math.pow(1 + ((interest / 100) / 12), years * 12);
    		//Format resulting string
    		lbResult.setTextFill(Color.BLACK);
    		tfResult.setText(String.format( "%.2f", result ));
    		
    	} else {
    		lbResult.setTextFill(Color.RED);
    		tfResult.setText("Please fill in the above.");
    	}
    }
    
    public double parseDoubleIfPossible(String toParse) {
      double parsed = 0;
      try {
    	  parsed = Double.parseDouble(toParse);
	  }  
	  catch(NumberFormatException nfe) {
		  System.out.println("Could not format the string!");
	  }
      return parsed;
    }
    
}
